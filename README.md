# Projects Manager

Juggle focus on different projects with this smart and fair management system.

## Todo
- General code tidying
- `state` module that will be able to check the state before carrying out an action
- Resuming and setting the remaining time proportional to the remaining time of others
- Being able to stash to focus on a specific project
- Being able to edit the times
- Adding to the list of upcoming events

## Journal
- Works on different files, with different purposes, with different programs
- `create` to create a project
	- This will execute the following commands given `./create project-name minutes end-date`
	- Navigate to directory as stored in CONFIG
	- Call `git init project-name`
	- Create README.md
	- Pass "# project-name" into README.md
	- Create LICENSE
	- Pass in GPLv3, with its name changed to project-name and person name changed to that in CONFIG
	- Add project-name,minutes,end-date to DEADLINES.csv in position
- `get` to get next project-time-percentage
	- Syntax: `./get time perc`
	- Get project from CURRENT file, and replace time with (100-perc)/perc * time, deleting if perc is 100
	- Filter those in DEADLINES.csv with the field "not done today" still unchecked
	- Total the minutes each one should take in a day, using dates, t
	- Take total number of minutes left for day using UPCOMING.csv, final time as stored in CONFIG, d
	- Select x, the number of minutes a randomly selected non-done project should take, and p, the percentage it will take
	- x * d/t = amount of time to assign
	- Write project, start end time to CURRENT file
	- Echo the end time
- `pause` to pause the current timing of the project
	- Replace start and end time with time to go in the CURRENT file
- `resume` to continue the project
	- Replace the start time with the current time, end time with start time plus time-to-go
	- Echo the end time
- `goto` to go to a project
	- Syntax: `./goto project-name`
	- Navigate to directory is stored in config
	- Then to project name
- `new` to begin a new day
	- Simply set the "done today field" to 0 in DEADLINES.csv
	- Give the first project
- `remove` to remove a project from the list
