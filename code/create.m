%--------------------------------------------------%
% vim: ts=4 sw=4 et ft=mercury
%--------------------------------------------------%
% Copyright (C) Matthew Bardsley.
% This file is distributed under the terms specified in README.md.
%--------------------------------------------------%
%
% File: create.m.
% Main authors: mhbardsley.
% Stability: high.
%
% This module provides the predicates required to create a new project.
%
%--------------------------------------------------%
%--------------------------------------------------%

:- module create.
:- interface.

:- import_module io.

%--------------------------------------------------%
%
% The main predicate that interfaces with I/O.
%

% Only one main predicate is needed, since other modules do not interact
% with this one.

	% main(!IO)
	%
	% True iff program will interface with I/O to add a project to
	% the global config files.
:- pred main(io::di, io::uo) is cc_multi.

%--------------------------------------------------%
%--------------------------------------------------%

:- implementation.

:- import_module types.
:- import_module string.
:- import_module float.
:- import_module calendar.
:- import_module bool.
:- import_module list. 
:- import_module exception.
:- import_module int.
:- import_module file_ops.

%--------------------------------------------------%

main(!IO) :-
	( try [io(!IO)] (
		get_arg_vals(ProjectName, Minutes, EndDate, Init, !IO),
		add_project(ProjectName, Minutes, EndDate, !IO),
		(
			Init = yes,
			form_git_dir(ProjectName, !IO)
		;
			Init = no
		)
	)
	then
		io.format("Successfully added project %s\n", 
			[s(ProjectName)], !IO)
	catch S ->
		io.format("User error: %s\n", [s(S)], !IO)
	).

:- pred get_arg_vals(string::out, float::out, date::out, bool::out, 
	io::di, io::uo) is cc_multi.
get_arg_vals(Name, Minutes, EndDate, Init, !IO) :-
	command_line_arguments(Args, !IO),
	( if
	  	Args = [PName, PMins, PDate, PInit]
	then
		( try [io(!IO)] (
			check_name(PName, !IO),
			check_minutes(PMins, PM),
			check_date(PDate, PD, !IO),
			check_init(PInit, PI),
			check_in_directory(PName, PI, !IO)
		)
		then
			Name = PName,
			Minutes = PM,
			EndDate = PD,
			Init = PI
		)
	else
		throw("four arguments not provided")
	).

:- pred check_name(string::in, io::di, io::uo) is cc_multi.
check_name(Name, !IO) :-
	( try [io(!IO)] (
		check_whitespace(Name),
		check_not_existing(Name, !IO)
	)
	then
		true
	).

:- pred check_whitespace(string::in) is det.
check_whitespace(String) :-
	to_char_list(String, CharList),
	( if
	  	member(' ', CharList)
	then
		throw("project name contains whitespace")
	else
		true
	).

:- pred check_not_existing(string::in, io::di, io::uo) is det.
check_not_existing(Name, !IO) :-
	read_direct_term("projects", Deadlines, !IO),
	( if
	  	member(Deadline, Deadlines),
		Deadline^d_name = Name
	then
		throw("project already exists")
	else
		true
	).

:- pred check_minutes(string::in, float::out) is det.
check_minutes(String, Float) :-
	( if
	  	to_float(String, FloatRep)
	then
		( if
		  	FloatRep > 0.0
		then
			Float = FloatRep
		else
			throw("minutes argument is not above 0")
		)
	else
		throw("minutes argument is not an float")
	).

:- pred check_date(string::in, date::out, io::di, io::uo) is det.
check_date(String, Date, !IO) :-
	( if
	  	date_from_string(String ++ " 00:00:00", DateRep)
	then
		current_local_time(Current, !IO),
		Duration = day_duration(Current, DateRep),
		( if
		  	days(Duration) >= 0
		then
			Date = DateRep
		else
			throw("date provided is too early")
		)
	else
		throw("date argument does not have correct format")
	).

:- pred check_init(string::in, bool::out) is det.
check_init(InitStr, Init) :-
	( if
	  	InitStr = "yes"
	then
		Init = yes
	else if
		InitStr = "no"
	then
		Init = no
	else
		throw("init argument does not have correct format")
	).

:- pred check_in_directory(string::in, bool::in, io::di, io::uo) is det.
check_in_directory(Name, Bool, !IO) :-
	( if
	  	Bool = yes
	then
		read_direct_term("directory", Directory, !IO),
		io.call_system("ls " ++ Directory ++ "/" ++ Name ++
			" > /dev/null 2>&1", Res, !IO),
		(
		 	Res = ok(Int),
			( if
			  	Int = 2
			then
				true
			else if
				Int = 0
			then
				throw("project directory already exists")
			else 
				throw("issue checking project directory")
			)
		;
			Res = error(ErrorCode),
			io.error_message(ErrorCode, Error),
			Message = "when checking project directory, "
				++ Error,
			throw(Message)
		)
	else
		true
	).

:- pred add_project(string::in, float::in, date::in, io::di, io::uo) is det.
add_project(Name, Mins, Date, !IO) :-
	read_direct_term("projects", ExistingProjects, !IO),
	NewProjects = [deadline(Name, Mins, Date, no) | ExistingProjects],
	open_output_write("projects", Stream, !IO),
	write_direct_term(Stream, NewProjects, !IO).

:- pred form_git_dir(string::in, io::di, io::uo) is cc_multi.
form_git_dir(Name, !IO) :-
	( try [io(!IO)] (
		create_dir(Name, !IO),
		create_files(Name, !IO)
	)
	then
		true
	).

:- pred create_dir(string::in, io::di, io::uo) is cc_multi.
create_dir(Name, !IO) :-
	( try [io(!IO)] (
		get_single_config("directory", Directory, !IO)
	)
	then
		ProjectLocation = Directory ++ "/" ++ Name,
		io.call_system("git init " ++ ProjectLocation,	InitRes, !IO),
		(
			InitRes = io.ok(Code),
			( if
				Code \= 0
			then
				throw("could not initialise directory")
			else
				true
			)
		;
			InitRes = io.error(ErrorCode),
			error_message(ErrorCode, Message),
			throw("initialising git, " ++ Message)
		)
	).

:- pred get_single_config(string::in, string::out, io::di, io::uo) is det.
get_single_config(Name, Result, !IO) :-
	io.open_input("../config/" ++ Name, OpenRes, !IO),
	(
		OpenRes = io.ok(Stream),
		io.read(Stream, Res, !IO),
		io.close_input(Stream, !IO),
		(
			Res = io.ok(Result)
		;
			Res = io.eof,
			throw(Name ++ " config file empty")
		;
			Res = io.error(Message, Line),
			int_to_string(Line, LineStr),
			Error = Message ++ " at line " ++ LineStr,
			throw("reading " ++ Name ++ ", " ++ Error)
		)
	;
		OpenRes = io.error(ErrorCode),
		error_message(ErrorCode, Message),
		throw("opening " ++ Name ++ ", " ++ Message)
	).

:- pred create_files(string::in, io::di, io::uo) is cc_multi.
create_files(Name, !IO) :-
	( try [io(!IO)] (
		create_readme(Name, !IO)
	)
	then
		T1 = 1
	catch S1 ->
		io.format("Warning: %s\n", [s(S1)], !IO),
		T1 = 0
	),
	( try [io(!IO)] (
		create_license(Name, !IO)
	)
	then
		T2 = T1 + 1
	catch S2 ->
		io.format("Warning: %s\n", [s(S2)], !IO),
		T2 = T1
	),
	( if
		T2 > 0
	then
		do_commit(Name, !IO)
	else
		true
	).

:- pred create_readme(string::in, io::di, io::uo) is cc_multi.
create_readme(Name, !IO) :-
	( try [io(!IO)] (
		get_single_config("directory", Directory, !IO)
	)
	then
		Path = Directory ++ "/" ++ Name ++ "/README.md",
		( try [io(!IO)] (
			create_empty_file(Path, !IO)
		)
		then
			io.open_output(Path, Res, !IO),
			(
				Res = io.ok(Stream),
				io.write_string(Stream, "# " ++ Name, !IO),
				io.close_output(Stream, !IO)
			;
				Res = io.error(ErrorCode),
				error_message(ErrorCode, Message),
				throw("writing to " ++ Path ++ ", " ++ Message)
			)
		)
	).

:- pred create_empty_file(string::in, io::di, io::uo) is det.
create_empty_file(Path, !IO) :-
	io.call_system("touch " ++ Path, Res, !IO),
	(
		Res = ok(Int),
		( if
			Int \= 0
		then
			throw("could not create " ++ Path)
		else
			true
		)
	;
		Res = error(ErrorCode),
		error_message(ErrorCode, Message),
		throw("creating " ++ Path ++ ", " ++ Message)
	).

:- pred create_license(string::in, io::di, io::uo) is cc_multi.
create_license(Name, !IO) :-
	( try [io(!IO)] (
		get_single_config("template", Template, !IO),
		write_license(Name, Template, !IO)
	)
	then
		true
	).

:- pred write_license(string::in, string::in, io::di, io::uo) is cc_multi.
write_license(Name, Template, !IO) :-
	( try [io(!IO)] (
		get_single_config("directory", Directory, !IO),
		get_single_config("author", Author, !IO)
	)
	then
		current_local_time(Current, !IO),
		int_to_string(year(Current), Year),
		replace_all(Template, "<author>", Author, Temp1),
		replace_all(Temp1, "<year>", Year, Temp2),
		replace_all(Temp2, "<name>", Name, License),
		Path = Directory ++ "/" ++ Name ++ "/LICENSE",
		( try [io(!IO)] (
			create_empty_file(Path, !IO)
		)
		then
			io.open_output(Path, Res, !IO),
			(
				Res = io.ok(Stream),
				io.write_string(Stream, License, !IO),
				io.close_output(Stream, !IO)
			;
				Res = io.error(ErrorCode),
				error_message(ErrorCode, Message),
				throw("writing to " ++ Path ++ ", " ++ Message)
			)
		)
	).
	

:- pred do_commit(string::in, io::di, io::uo) is cc_multi.
do_commit(Name, !IO) :-
	( try [io(!IO)] (
		get_single_config("directory", Directory, !IO)
	)
	then
		Commands = "cd " ++ Path ++ "; git add -A; "
			++ "git commit -a -m ""Initial commit""",
		Path = Directory ++ "/" ++ Name,
		io.call_system(Commands, Res, !IO),
		(
			Res = ok(Int),
			( if
				Int \= 0
			then
				throw("git commit failed")
			else
				true
			)
		;
			Res = error(ErrorCode),
			error_message(ErrorCode, Error),
			throw("running git commit, " ++ Error)
		)
	).
