/* Projects Manager - File Ops
* Author: Matthew Bardsley
*/

:- module file_ops.
:- interface.
:- import_module io.
:- pred read_direct_term(string::in, T::out, io::di, io::uo) is det.
:- pred open_output_write(string::in, io.text_output_stream::out, io::di, 
	io::uo) is det.
:- pred open_output_append(string::in, io.text_output_stream::out, io::di, 
	io::uo) is det.
:- pred write_direct_term(io.text_output_stream::in, T::in, io::di, io::uo) 
	is det.
:- implementation.
:- import_module string, exception.

read_direct_term(File, Term, !IO) :-
	io.open_input("../config/" ++ File, StreamRes, !IO),
	(
		StreamRes = ok(Stream),
		io.read(Stream, Res, !IO),
		io.close_input(Stream, !IO),
		(
			Res = ok(Term)
		;
			Res = eof,
			make_error_message("read", File, "no data present")
		;
			Res = error(Message, Line),
			make_error_message("read", File, Message ++ " on line " 
				++ LineStr),
			int_to_string(Line, LineStr)
		)
	;
		StreamRes = error(ErrorCode),
		io.error_message(ErrorCode, Error),
		make_error_message("read", File, Error)
	).

:- pred make_error_message(string::in, string::in, string::in) is erroneous.
make_error_message(TypeOfAccess, File, Error) :-
	Message = "when trying to " ++ TypeOfAccess ++ " " ++ File ++ 
		", " ++ Error,
	throw(Message).

open_output_write(File, Stream, !IO) :-
	io.open_output("../config/" ++ File, StreamRes, !IO),
	(
		StreamRes = ok(Stream)
	;
		StreamRes = error(ErrorCode),
		io.error_message(ErrorCode, Error),
		make_error_message("write", File, Error)
	).

open_output_append(File, Stream, !IO) :-
	io.open_append("../config/" ++ File, StreamRes, !IO),
	(
		StreamRes = ok(Stream)
	;
		StreamRes = error(ErrorCode),
		io.error_message(ErrorCode, Error),
		make_error_message("append", File, Error)
	).

write_direct_term(Stream, Term, !IO) :-
	io.write(Stream, Term, !IO),
	io.write_string(Stream, ".\n", !IO),
	io.close_output(Stream, !IO).

:- end_module file_ops.

