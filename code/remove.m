/* Projects Manager - Remove
* Author: Matthew Bardsley
*/

:- module remove.
:- interface.
:- import_module io.
:- pred main(io, io).
:- mode main(di, uo) is cc_multi.
:- implementation.
:- import_module file_ops, types, list, maybe, string, exception.

main(!IO) :-
	( try [io(!IO)] (
		get_args(ProjectName, !IO),
		read_direct_term("projects", Projects, !IO),
		read_direct_term("current", Current, !IO),
		open_output_write("projects", PStream, !IO),
		open_output_write("current", CStream, !IO)
	)
	then
		delete_projects(ProjectName, Projects, NewProjects),
		( if
		  	Current = yes(started(ProjectName, _, _))
			;
			Current = yes(paused(ProjectName, _))
		then
			NewCurrent = no
		else
			Current = NewCurrent
		),
		write_direct_term(PStream, NewProjects, !IO),
		write_direct_term(CStream, NewCurrent, !IO),
		io.format("Removed %s if it exists\n", [s(ProjectName)], !IO)
	catch S ->
		io.format("User error: %s\n", [s(S)], !IO)
	).

:- pred get_args(string::out, io::di, io::uo) is det.
get_args(ProjectName, !IO) :-
	io.command_line_arguments(Args, !IO),
	( if
	  	Args = [PName]
	then
		ProjectName = PName
	else
		throw("single argument not provided")
	).

:- pred delete_projects(string::in, list(deadline)::in, 
	list(deadline)::out) is multi.
delete_projects(Name, Deadlines, NewDeadlines) :-
	( if
		delete(Deadlines, X, Rest)
	then
		delete_projects(Name, Rest, New),
		( if
	 		X^d_name = Name
		then
			NewDeadlines = New
		else
			insert(X, New, NewDeadlines)
		)
	else
		NewDeadlines = Deadlines
	).
