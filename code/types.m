/* Projects Manager - Types
* Author: Matthew Bardsley
*/

:- module types.
:- interface.
:- import_module string, float, calendar, bool.
:- type deadline
	--->	deadline(
			d_name :: string, 
			d_minutes :: float, 
			d_enddate :: date,
			d_donetoday :: bool
		).

:- type current
        --->    started(
                        s_name :: string,
                        s_start :: date,
			s_remaining :: duration
                )
	;
		paused(
                        p_name :: string,
                        p_remaining :: duration
                ).

:- type upcoming
	--->	upcoming(
			up_name :: string,
			up_start :: date,
			up_end :: date
		).

:- implementation.
:- end_module types.

