/* Projects Manager - Pause
* Author: Matthew Bardsley
*/

:- module pause.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.
:- implementation.
:- import_module types, file_ops, require, calendar, string, list, 
	exception, time_conversions, maybe.

main(!IO) :-
	( try [io(!IO)] (
		read_direct_term("current", Current, !IO),
		open_output_write("current", Stream, !IO),
		check_started(Stream, Current, Started, !IO)
	)
	then
		current_local_time(CurrentTime, !IO),
		started_to_paused(CurrentTime, Started, Paused),
		write_direct_term(Stream, yes(Paused), !IO),
		( if
		  	Paused = paused(Name, Duration)
		then
			get_duration_minutes(Duration, Minutes),
			io.format("Paused %s with %f minutes to go\n",
				[s(Name), f(Minutes)], !IO)
		else
			unexpected($pred, "Paused is not of sub-type paused")
		)
	catch S ->
		io.format("User error: %s\n", [s(S)], !IO)
	).

:- pred check_started(io.text_output_stream::in, maybe(current)::in, 
	current::out, io::di, io::uo) is det.
check_started(Stream, Current, Started, !IO) :-
	( if
	  	Current = yes(S),
		S = started(_, _, _)
	then
		Started = S
	else
		write_direct_term(Stream, Current, !IO),
		( if
		  	Current = no
		then
			throw("no task in progress")
		else
			throw("task already paused")
		)
	).

:- pred started_to_paused(date::in, current::in, current::out) is det.
started_to_paused(CurrentTime, Started, paused(NewName, NewRemaining)) :-
	( if
	 	Started = started(Name, Start, Remaining)
	then
		NewName = Name,
		get_end(Start, Remaining, End),
		get_remaining(CurrentTime, End, NewRemaining)
	else
		unexpected($pred, "Started is not of sub-type started")
	).

:- pred get_end(date::in, duration::in, date::out) is det.
get_end(Start, Remaining, End) :-
	add_duration(Remaining, Start, End).

:- pred get_remaining(date::in, date::in, duration::out) is det.
get_remaining(CurrentTime, End, NewRemaining) :-
	NewRemaining = duration(CurrentTime, End).
