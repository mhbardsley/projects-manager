/* Projects Manager - Get
* Author: Matthew Bardsley
*/

:- module get.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.
:- implementation.
:- import_module types, list, calendar, float, string, exception, int,
	bool, require, file_ops, time_conversions, maybe.
:- use_module random.

main(!IO) :-
	( try [io(!IO)] (
		check_arguments(Time, Perc, !IO),
		get_current(MaybeCurrent, !IO),
		get_deadlines(Deadlines, !IO),
		get_upcoming(Upcoming, !IO),
		get_final_time(FinalTime, !IO),
		open_output_streams(DeadlineStream, CurrentStream, !IO)
	)
	then
		( if
		  	MaybeCurrent = yes(Current)
		then
			extract_project_name(Current, Name),
			create_new_deadlines(Deadlines, Name, Time, 
				Perc, NewDeadlines)
		else
			NewDeadlines = Deadlines
		),
		write_direct_term(DeadlineStream, NewDeadlines, !IO),
		filter_not_done(NewDeadlines, NotDone),
		(
			NotDone = [_ | _],
			get_total_minutes(NotDone, T, !IO),
			get_total_mins_remaining(Upcoming, FinalTime, D, !IO),
			select_random(NotDone, RandomProject),
			get_name_time_perc(RandomProject, NewName, X, P, !IO),
			X * D / T = TimeToAssign,
			get_start_end(TimeToAssign, Start, End, !IO),
			create_current(NewName, Start, TimeToAssign, 
				NewCurrent),
			write_current(CurrentStream, yes(NewCurrent), !IO),
			get_string_date(End, StringEnd),
			io.format("Now complete %f%% of %s for %f " ++
				"minutes (current goal %s)\n",
				[f(P), s(NewName), f(TimeToAssign), 
				s(StringEnd)], !IO)
		;
			NotDone = [],
			write_current(CurrentStream, no, !IO),
			io.format("Done all tasks for the day\n", [], !IO)
		),
		io.close_output(CurrentStream, !IO)
	catch S ->
		io.format("User error: %s\n", [s(S)], !IO)
	).


:- pred check_arguments(float::out, float::out, io::di, io::uo) is cc_multi.
check_arguments(Mins, Perc, !IO) :-
	io.command_line_arguments(Args, !IO),
	( if
	  	Args = [M, P]
	then
		( try [io(!IO)] (
			attempt_to_float(M, MFloat),
			attempt_to_float(P, PFloat)
		)
		then
			( if
			  	MFloat > 0.0
			then
				Mins = MFloat
			else
				throw("minutes given invalid")
			),
			( if
			  	PFloat > 0.0, PFloat =< 100.0
			then
				Perc = PFloat
			else
				throw("percentage given invalid")
			)
		)
	else
		throw("need to provide two arguments")
	).

:- pred attempt_to_float(string::in, float::out) is det.
attempt_to_float(String, Float) :-
	( if
	  	to_float(String, F)
	then
		F = Float
	else
		throw("floating point arguments not used")
	).

:- pred get_current(maybe(current)::out, io::di, io::uo) is det.
get_current(Current, !IO) :-
	io.open_input("../config/current", StreamRes, !IO),
	(
	 	StreamRes = ok(Stream),
		io.read(Stream, Res, !IO),
		io.close_input(Stream, !IO),
		(
		 	Res = ok(Cur),
			check_current(Cur, !IO),
			Cur = Current
		;
			Res = eof,
			throw("current file is empty")
		;
			Res = error(Message, Line),
			int_to_string(Line, LineStr),
			Error = "when reading current, " ++ Message
				++ " on line " ++ LineStr,
			throw(Error)
		)
	;
		StreamRes = error(ErrorCode),
		error_message(ErrorCode, Message),
		Error = "when opening current to read, " ++ Message,
		throw(Error)
	).

:- pred check_current(maybe(current)::in, io::di, io::uo) is det.
check_current(Current, !IO) :-
		Current = no
	;
	((Current = yes(started(Name, _, _)) ; Current = yes(paused(Name, _))),
	get_deadlines(Deadlines, !IO),
	( if
		Deadline = deadline(Name, _, _, _),
		member(Deadline, Deadlines)
	then
		true
	else
		throw("could not find any current")
	)
	).

:- pred get_deadlines(list(deadline)::out, io::di, io::uo) is det.
get_deadlines(Deadlines, !IO) :-
	read_direct_term("projects", Projects, !IO),
	current_local_time(Current, !IO),
	(if
	  	member(Project, Projects),
		Project^d_minutes =< 0.0
	then
		throw("found a deadline with non-positive minutes")
	else if
		member(Project, Projects),
		Days = day_duration(Current, Project^d_enddate),
		days(Days) < 0
	then
		throw("found a deadline with end-date too early")
	else if
		member(Project1, Projects),
		member(Project2, Projects),
		Project1 \= Project2,
		Project1^d_name = Project2^d_name
	then
		throw("multiple projects with same name found")
	else
		Deadlines = Projects
	).

:- pred get_upcoming(list(upcoming)::out, io::di, io::uo) is det.
get_upcoming(UpcomingList, !IO) :-
	read_direct_term("upcoming", Upcomings, !IO),
	current_local_time(Current, !IO),
	( if
	  	member(Upcoming, Upcomings),
		date_leq(Upcoming^up_end, Upcoming^up_start)
	then
		throw("found an event that starts before it ends")
	else if
		member(Upcoming, Upcomings),
		date_leq(Upcoming^up_end, Current)
	then
		throw("found an event that has already happened")
	else
		UpcomingList = Upcomings
	).

:- pred get_final_time(date::out, io::di, io::uo) is det.
get_final_time(Date, !IO) :-
	current_local_time(Time, !IO),
	unpack_date(Time, Year, Month, Day, _, _, _, _),
	ADay = init_duration(0, 0, 1, 0, 0, 0, 0),
	Start = det_init_date(Year, Month, Day, 0, 0, 0, 0),
	add_duration(ADay, Start, Date).

:- pred open_output_streams(io.text_output_stream::out, 
	io.text_output_stream::out, io::di, io::uo) is det.
open_output_streams(DeadlineStream, CurrentStream, !IO) :-
	io.open_output("../config/projects", DStreamRes, !IO),
	(
	 	DStreamRes = ok(DeadlineStream)
	;
		DStreamRes = error(ErrorCode),
		error_message(ErrorCode, Message),
		Error = "when opening projects to write, " ++ Message,
		throw(Error)
	),
	io.open_output("../config/current", CStreamRes, !IO),
	(
	 	CStreamRes = ok(CurrentStream)
	;
		CStreamRes = error(ErrorCode),
		error_message(ErrorCode, Message),
		Error = "when opening current to write, " ++ Message,
		throw(Error)
	).

:- pred extract_project_name(current::in, string::out) is det.
extract_project_name(started(Name, _, _), Name).
extract_project_name(paused(Name, _), Name).

:- pred create_new_deadlines(list(deadline)::in, string::in, float::in, 
	float::in, list(deadline)::out) is cc_multi.
create_new_deadlines(Deadlines, Name, Time, Perc, NewDeadlines) :-
	( if
	  	member(deadline(Name, Mins, EndDate, no), Deadlines)
	then
		( if
	  		Perc \= 100.0
		then
			NewMinutes = Time * (100.0 - Perc)/Perc,
			NewDeadline = deadline(Name, NewMinutes, EndDate, yes),
			replace_all(Deadlines, 
				deadline(Name, Mins, EndDate, no), 
				NewDeadline, NewDeadlines)
		else
			delete_all(Deadlines, 
				deadline(Name, Mins, EndDate, no), 
				NewDeadlines)
		)
	else
		error("create_new_deadlines")
	).

:- pred write_deadlines(io.text_output_stream::in, list(deadline)::in, io::di,
	io::uo) is det.
write_deadlines(Stream, Deadlines, !IO) :-
	foldl(write_deadline(Stream), Deadlines, !IO).

:- pred write_deadline(io.text_output_stream::in, deadline::in, io::di, io::uo)
	is det.
write_deadline(Stream, Deadline, !IO) :-
	io.write(Stream, Deadline, !IO),
	io.write_string(Stream, ".\n", !IO).

:- pred filter_not_done(list(deadline)::in, list(deadline)::out) is det.
filter_not_done(Deadlines, ThoseNotDone) :-
	filter(not_done, Deadlines, ThoseNotDone).

:- pred not_done(deadline::in) is semidet.
not_done(deadline(_, _, _, no)).

:- pred get_total_minutes(list(deadline)::in, float::out, io::di, 
	io::uo) is det.
get_total_minutes(Deadlines, Minutes, !IO) :-
	foldl2(add_minutes, Deadlines, 0.0, Minutes, !IO).

:- pred add_minutes(deadline::in, float::in, float::out, io::di, io::uo) is det.
add_minutes(deadline(_, Mins, Date, _), Float, Float + NewMins, !IO) :-
	current_local_time(Current, !IO),
	DaysBetween = julian_day_number(Date) - julian_day_number(Current),
	NewMins = Mins / float(DaysBetween).

:- pred get_total_mins_remaining(list(upcoming)::in, date::in, float::out,
	io::di, io::uo) is det.
get_total_mins_remaining(Upcoming, FinalTime, Remaining, !IO) :-
	current_local_time(Current, !IO),
	get_disruption(Upcoming, Current, FinalTime, Disruption),
	Between = duration(Current, FinalTime),
	get_duration_minutes(Between, TotalMins),
	Remaining = TotalMins - Disruption.

:- pred get_disruption(list(upcoming)::in, date::in, date::in,
	float::out) is det.
get_disruption(UpcomingList, Current, FinalTime, Result) :-
	filter(is_candidate_upcoming(FinalTime), UpcomingList, Candidates),
	foldl(get_single_disruption(Current, FinalTime), Candidates, 0.0, 
		Result).

:- pred is_candidate_upcoming(date::in, upcoming::in) is semidet.
is_candidate_upcoming(FinalTime, upcoming(_, Start, _)) :-
	date_leq(Start, FinalTime).

:- pred get_single_disruption(date::in, date::in, upcoming::in, float::in, 
	float::out) is det.
get_single_disruption(Current, Final, upcoming(_, Start, End), FloatIn, FloatOut) :-
	StartDuration = day_duration(Start, Current),
	( if
	  	duration_leq(StartDuration, zero_duration)
	then
		ActualStart = Start
	else
		ActualStart = Current
	),
	EndDuration = day_duration(End, Final),
	( if
	  	duration_leq(EndDuration, zero_duration)
	then
		ActualEnd = Final
	else
		ActualEnd = End
	),
	ActualDuration = day_duration(ActualStart, ActualEnd),
	FloatOut = float(hours(ActualDuration) * 60) + 
		float(minutes(ActualDuration)) 
		+ float(seconds(ActualDuration)) / 60.0
		+ FloatIn.

:- pred select_random(list(deadline)::in, deadline::out) is det.
select_random(Deadlines, Deadline) :-
	some [!RS] (
		random.init(seed, !:RS),
		random.random(0, length(Deadlines), Index, !.RS, _)
	),
	det_index0(Deadlines, Index, Deadline).

:- pred get_name_time_perc(deadline::in, string::out, float::out, 
	float::out, io::di, io::uo) is det.
get_name_time_perc(deadline(Name, Mins, EndDate, _), Name, Time, Perc, !IO) :-
	current_local_time(Current, !IO),
	DaysBetween = float(julian_day_number(EndDate) - 
		julian_day_number(Current)),
	Time = Mins / DaysBetween,
	Perc = float(100) / DaysBetween.

:- pred get_start_end(float::in, date::out, date::out, io::di, io::uo) is det.
get_start_end(TimeToAssign, Start, End, !IO) :-
	current_local_time(Start, !IO),
	Duration = init_duration(0, 0, 0, 0, 
		ceiling_to_int(TimeToAssign), 0, 0),
	add_duration(Duration, Start, End).

:- pred create_current(string::in, date::in, float::in, current::out) is det.
create_current(Name, Start, TimeToAssign, started(Name, Start,
	init_duration(0, 0, 0, 0, ceiling_to_int(TimeToAssign), 0, 0))).

:- pred write_current(io.text_output_stream::in, maybe(current)::in, 
	io::di, io::uo) is det.
write_current(Stream, Current, !IO) :-
	io.write(Stream, Current, !IO),
	io.write_string(Stream, ".\n", !IO).

:- pred close_output_streams(io.text_output_stream::in, 
	io.text_output_stream::in, io::di, io::uo) is det.
close_output_streams(DeadlineStream, CurrentStream, !IO) :-
	io.close_output(DeadlineStream, !IO),
	io.close_output(CurrentStream, !IO).

:- mutable(seed, int, 0, ground, [untrailed]).
:- initialise init_seed/0.

:- pragma promise_pure seed/0.
:- func seed = int.
seed = N :-
	semipure get_seed(N).

:- impure pred init_seed is det.
init_seed :-
	impure Time = epochtime,
	impure set_seed(Time).

:- pragma foreign_decl("C", "#include <time.h>").

:- impure func epochtime = int.
:- pragma foreign_proc("C",
	epochtime = (Time::out),
	[will_not_call_mercury],
"
	Time = time(NULL);
").
