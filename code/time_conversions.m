/* Projects Manager - Time Conversions
* Author: Matthew Bardsley
*/

:- module time_conversions.
:- interface.
:- import_module calendar, float, string.
:- pred get_duration_minutes(duration::in, float::out) is det.
:- pred get_string_date(date::in, string::out) is det.
:- pred date_leq(date::in, date::in) is semidet.
:- implementation.

get_duration_minutes(Duration, Result) :-
	Result = float(microseconds(Duration)) / 60000000.0
		+ float(seconds(Duration)) / 60.0
		+ float(minutes(Duration))
		+ float(hours(Duration)) * 60.0
		+ float(days(Duration)) * 60.0 * 24.0
		% note this will now provide just an average
		+ float(months(Duration)) * 60.0 * 24.0 * 30.0
		+ float(years(Duration)) * 60.0 * 24.0 * 30.0 * 12.0.

get_string_date(Date, String) :-
        int_to_string(hour(Date), Hour),
        int_to_string(minute(Date), Minute),
        int_to_string(second(Date), Second),
        String = Hour ++ ":" ++ Minute ++ ":" ++ Second.

date_leq(Date1, Date2) :-
	Duration = duration(Date1, Date2),
	duration_leq(zero_duration, Duration).

:- end_module time_conversions.
