/* Projects Manager - Goto
* Author: Matthew Bardsley
*/

:- module goto.
:- interface.
:- import_module io.
:- pred main(io, io).
:- mode main(di, uo) is cc_multi.
:- implementation.
:- import_module file_ops, list, string, exception.

main(!IO) :-
	( try [io(!IO)] (
		read_direct_term("directory", Directory, !IO),
		get_project(Directory, ProjectName, !IO),
		read_direct_term("command", Command, !IO),
		execute_command(Command, Directory, ProjectName, !IO)
	)
	then
		io.format("Navigated to %s successfully\n", 
			[s(ProjectName)], !IO)
	catch S ->
		io.format("User error: %s\n", [s(S)], !IO)
	).

:- pred get_project(string, string, io, io).
:- mode get_project(in, out, di, uo) is det.
get_project(Directory, ProjectName, !IO) :-
	io.command_line_arguments(Args, !IO),
	( if
	  	Args = [PN]
	then
		io.call_system("ls " ++ Directory ++ "/" ++ 
			PN ++ " > /dev/null 2>&1", Res, !IO),
		(
		 	Res = ok(Int),
			( if
			  	Int = 0
			then
				ProjectName = PN
			else if
				Int = 2
			then
				throw("project directory does not exist")
			else
				throw("issue checking project directory")
			)
		;
			Res = error(ErrorCode),
			io.error_message(ErrorCode, Error),
			Message = "when checking project directory, " ++ Error,
			throw(Message)
		)
	else
		throw("one argument has not been supplied")
	).

:- pred execute_command(string, string, string, io, io).
:- mode execute_command(in, in, in, di, uo) is det.
execute_command(Command, Directory, ProjectName, !IO) :-
	NewCommand = Command ++ Directory ++ "/" ++ ProjectName ++ " &",
	io.call_system(NewCommand, Res, !IO),
	(
	 	Res = ok(Int),
		( if
		  	Int \= 0
		then
			throw("issue accessing project directory")
		else
			true
		)
	;
		Res = error(ErrorCode),
		io.error_message(ErrorCode, Error),
		Message = "when accessing project directory, " ++ Error,
		throw(Message)
	).
