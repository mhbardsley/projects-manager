/* Projects Manager - New
* Author: Matthew Bardsley
*/

:- module new.
:- interface.
:- import_module io.
:- pred main(io, io).
:- mode main(di, uo) is cc_multi.
:- implementation.
:- import_module file_ops, list, string, types, bool, exception.

main(!IO) :-
	( try [io(!IO)] (
		read_direct_term("projects", Projects, !IO),
		open_output_write("projects", Stream, !IO),
		open_output_write("current", CurStream, !IO)
	)
	then
		write_direct_term(CurStream, no, !IO),
		set_no(Projects, NewProjects),
		write_direct_term(Stream, NewProjects, !IO),
		io.write_string("Successfully started new day\n", !IO),
		( try [io(!IO)] (
			call_get(!IO)
		)
		then
			true
		catch AnotherS ->
		io.format("User error: %s\n", [s(AnotherS)], !IO)
		)

	catch S ->
		io.format("User error: %s\n", [s(S)], !IO)
	).

:- pred set_no(list(deadline), list(deadline)).
:- mode set_no(in, out) is cc_multi.
set_no(Project, NewProject) :-
	( if
		delete(Project, X, OtherProjects)
	then
		set_no(OtherProjects, OtherNew),
		(X^d_donetoday := no) = Y,
		insert(Y, OtherNew, NewProject)
	else
		Project = NewProject
	).

:- pred call_get(io, io).
:- mode call_get(di, uo) is det.
call_get(!IO) :-
	io.call_system("./get 1 1", Res, !IO),
	(
	 	Res = ok(Int),
		( if
		  	Int = 0
		then
			true
		else
			throw("running get threw an error")
		)
	;
		Res = error(ErrorCode),
		io.error_message(ErrorCode, Error),
		Message = "when trying to call get, " ++ Error,
		throw(Message)
	).
