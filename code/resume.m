/* Projects Manager - Resume
* Author: Matthew Bardsley
*/

:- module resume.
:- interface.
:- import_module io.
:- pred main(io::di, io::uo) is cc_multi.
:- implementation.
:- import_module file_ops, calendar, types, exception, require, 
	string, list, time_conversions, maybe.

main(!IO) :-
	( try [io(!IO)] (
		read_direct_term("current", Current, !IO),
		open_output_write("current", Stream, !IO),
		check_paused(Stream, Current, Paused, !IO)
	)
	then
		current_local_time(LocalTime, !IO),
		get_started(LocalTime, Paused, Started),
		write_direct_term(Stream, yes(Started), !IO),
		( if
		  	Started = started(Name, StartDate, Duration)
		then
			get_duration_minutes(Duration, Minutes),
			add_duration(Duration, StartDate, EndDate),
			get_string_date(EndDate, StringDate),
			io.format("Resumed %s with %f minutes to " ++
				"go (current goal %s)\n",
				[s(Name), f(Minutes), s(StringDate)],
				!IO)
		else
			unexpected($pred, "Started is not of sub-type started")
		)
	catch S ->
		io.format("User error: %s\n", [s(S)], !IO)
	).

:- pred check_paused(io.text_output_stream::in, maybe(current)::in, 
	current::out, io::di, io::uo) is det.
check_paused(Stream, Current, Paused, !IO) :-
	( if
	  	Current = yes(P),
		P = paused(_, _)
	then
		Paused = P
	else
		write_direct_term(Stream, Current, !IO),
		( if
		  	Current = no
		then
			throw("no task in progress")
		else
			throw("task already resumed")
		)
	).

:- pred get_started(date::in, current::in, current::out) is det.
get_started(LocalTime, Current, Started) :-
	( if
	  	Current = paused(Name, Remaining)
	then
		Started = started(Name, LocalTime, Remaining)
	else
		unexpected($pred, "Started is not of sub-type paused")
	).
